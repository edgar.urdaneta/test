import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { toggleOverlay } from "./Overlay";
import "./Overlay.scss";

class Overlay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillReceiveProps(props) {}

  render() {
    const visible = this.props.home.menu ? "block" : "none";
    return (
      <div id="overlay" style={{ display: visible }}>
        <ul>
        <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/services">Services</a>
          </li>
          <li>
            <a href="/contact">Kontakt</a>
          </li>
          <li>
            <a href="/about">About</a>
          </li>
          <li>
            <a href="/publications">Publikation & Vorträge</a>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      // getServices
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Overlay);
