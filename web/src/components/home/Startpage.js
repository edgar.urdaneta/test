import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Carousel, Tabs, Card } from "antd";
import { bindActionCreators } from "redux";
import { getPage } from "./Home.duck";
import { connect } from "react-redux";
import "./Startpage.scss";

class Startpage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: null
    };
  }

  componentWillMount() {
    this.props.getPage("startpage");
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {
    this.setState({ content: props.home.startpage });
  }

  render() {
    if (this.state.content) {
      const content = this.state.content;
      return (
        <div className="startpage-grid">
          <div className="big-corner">
            <h1>{content.main_square_title}</h1>            
            <div className="cuadro">
              <h2>{content.main_square_subtitle}</h2>
              {content.main_square_text}
            </div>
          </div>
          <div className="right-up">
            <h2>{content.square1_title}</h2>
            <p>{content.square1_text}</p>
          </div>
          <div className="right-down">
            <h2>{content.square2_title}</h2>
            <p>{content.square2_text}</p>
          </div>
          <div className="down">
            <h2>{content.rectangle_title}</h2>
          </div>
          <div className="small-corner">
            <h2>{content.square3_title}</h2>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Startpage);
