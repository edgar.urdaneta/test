import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Carousel, Tabs, Card } from "antd";
import { bindActionCreators } from "redux";
import { getPage } from "./Home.duck";
import { connect } from "react-redux";
import "./About.scss";

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: 1
    };
  }

  componentWillMount() {    
    this.props.getPage("about_dr_helene_karmasin");
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {    
    this.setState({ content: props.home.about_dr_helene_karmasin });    
  }

  render() {
    if(this.state.content) {
      const content = this.state.content;
      console.log("content", content);
      return (
        <div className="about-grid">
          <div className="tl-q" dangerouslySetInnerHTML={{__html: content.upper_square}} />
          <div className="tr-q"></div>
          <div className="bl-q"></div>
          <div className="br-q" dangerouslySetInnerHTML={{__html: content.bottom_square}} />          
        </div>
      );
    } else {
      return null;      
    }    
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About);
