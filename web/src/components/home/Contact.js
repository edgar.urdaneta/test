import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Carousel, Tabs, Card } from "antd";
import { bindActionCreators } from "redux";
import { getPage } from "./Home.duck";
import { connect } from "react-redux";
import "./Contact.scss";

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: 1
    };
  }

  componentWillMount() {
    // this.props.getContact();
    this.props.getPage("kontakt");
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {
    this.setState({ content: props.home.kontakt });     
  }

  render() {
    if (this.state.content) {
      const content = this.state.content;      
      return (
        <div className="contact-grid">
          <div className="big-left">
            <div dangerouslySetInnerHTML={{ __html:content.paragraph1 }} />
            <br/><br/>
            <div dangerouslySetInnerHTML={{ __html:content.paragraph2 }} />            
          </div>          
          <div className="big-right">
            <div dangerouslySetInnerHTML={{ __html:content.paragraph3 }} />
            <br/><br/>
            <div dangerouslySetInnerHTML={{ __html:content.paragraph4 }} />
            <br/><br/>
            <div dangerouslySetInnerHTML={{ __html:content.paragraph5 }} />
            <br/><br/>
            <div dangerouslySetInnerHTML={{ __html:content.paragraph6 }} />
          </div>          
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);
