import axios from "axios";

const LOADING = "LOADING";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";
const TOGGLE_MENU = "TOGGLE_MENU";

const initialState = {
  startpage: null,
  behavioural_insights: null,
  semiotik: null,
  methode2: null,
  methode3: null,
  methode_4: null,
  methode5: null,
  about_dr_helene_karmasin: null,
  kontakt: null,
  publikationen_und_vortraege: null,
  loading: false,
  menu: false
};

const api_url = "http://176.9.41.13:8080";
const directus_token = "8koACB4FJRdshqQLMs8ejQdlrnR6YO7C";
// REDUCERS --- : Handle the result of the actions

export default (state = initialState, action) => {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true
      };
    case SUCCESS:
      state[action._page] = action._data;
      return {
        ...state,        
        loading: false
      };    
    case FAILURE:
      return {
        ...state,
        loading: false
      };   
    case TOGGLE_MENU:
      return {
        ...state,
        menu: !state.menu
      };
    default:
      return state;
  }
};

// ACTIONS --- : Perform a change, call or (as its name implies) an action or logic

function loading() {
  return { type: LOADING };
}
function success(_page, _data) {
  return { type: SUCCESS, _page, _data };
}
function failure(_data) {
  return { type: FAILURE };
}

export function getPage(_page) {  
  return dispatch => {
    dispatch(loading());
    return axios({
      method: "GET",      
      url: `${api_url}/api/1.1/tables/${_page}/rows/1?access_token=${directus_token}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${directus_token}`
      },
      json: true
    })
      .then(response => {        
        if(response.status === 200) {
          return dispatch(success(_page, response.data.data));        
        } else {
          return dispatch(failure());
        }
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function toggleMenu() {
  return { type: TOGGLE_MENU };
}