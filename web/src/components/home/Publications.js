import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Carousel, Tabs, Card } from "antd";
import { bindActionCreators } from "redux";
import { getPage } from "./Home.duck";
import { connect } from "react-redux";
import "./Publications.scss";

class Publications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: 1
    };
  }

  componentWillMount() {
    this.props.getPage("publikationen_und_vortraege");
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {
    this.setState({ content: props.home.publikationen_und_vortraege });
  }

  render() {
    if (this.state.content) {
      const content = this.state.content;
      console.log("content", content);
      return (
        <div id="publications">
          <div className="header">
            <h1>Publikationen</h1>
          </div>
          <div className="columns">
            <div className="left-side big">
              <div className="pub1" />
            </div>
            <div className="right-side small center">
              <br />
              <br />
              <h2 className="subtitle">Wahre Schönheit kommt von außen</h2>
            </div>
          </div>
          <div>
            <div className="pub2" />
          </div>
          <div className="columns">
            <div className="left-side small center">
              <br />
              <br />
              <h2 className="subtitle">Cultural Theory.</h2>
              <p>
                Ein neuer Ansatz für Kommunikation, Marketing und Management,
                Wien 2004
              </p>
            </div>
            <div className="right-side big">
              <div className="pub3" />
            </div>
          </div>
          <div className="columns">
            <div className="left-side big">
              <div className="pub4" />
            </div>
            <div className="right-side small center">
              <br />
              <br />
              <span style={{ fontWeight: "bold" }}>
                Die Entschlüsselung des Packungs codes.
              </span>
            </div>
          </div>
          <div className="title2">Vorträge</div>
          <div className="columns" style={{background: "#fff", padding: 30, paddingTop: 60}}>
            <div className="left-side small">              
              Es ist mir ein Anliegen, über meine spezifischen Zugang zu Märkten
              zu sprechen. Ich habe diese in vielen Projekten erprobt und kann
              über Erfolge und Miss- erfolge im Marketing und Kommunikationsalltag berichten.               
              <br/><br/>
              Es geht immer um die Psychologie von Menschen, um ihr Verhalten, um die vielen Faktoren, die darauf
              einwirken. Und es geht um Kommunikation: 
              <br/><br/>
              <strong>
              Wie kann ich Menschen emotional und rational ansprechen? 
              </strong>
              <br/><br/>
              <span style={{fontWeight: "bold", color: "#c80628"}}>
              Welche Zeichen und Codes sind wirksam? 
              </span>
              <br/><br/>
              <strong>
              Wie verändern sich diese Codes über die Zeit? 
              </strong>
              <br/><br/>
              <span style={{fontWeight: "bold", color: "#c80628"}}>
              Welche sind derzeit innovativ? Welche Strategien haben behavioral economics,
              die Verhaltenswissenschaften entwickelt, um gewünschtes Verhalten
              herbeizuführen?
              </span>
            </div>
            <div className="right-side big">
              <div className="pub5" />
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Publications);
