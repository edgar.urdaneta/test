import React from "react";
import { Route } from "react-router-dom";
import { Container, Row, Col } from "react-grid-system";
import Startpage from "./Startpage";
import Overlay from "./Overlay";
import Services from "./Services";
import Contact from "./Contact";
import About from "./About";
import Publications from "./Publications";

const HomeContainer = ({ match }) => (
  <Container>          
    <Route exact path="/" component={Startpage} />
    <Route exact path="/services" component={Services} />
    <Route exact path="/contact" component={Contact} />
    <Route exact path="/about" component={About} />
    <Route exact path="/publications" component={Publications} />
    <Overlay />
  </Container>
);

export default HomeContainer;
