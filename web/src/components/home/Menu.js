import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { toggleMenu } from "./Home.duck";
import "./Menu.scss";

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillReceiveProps(props) {
    console.log("props", props.home.menu);
  }

  render() {    
      if(this.props.home.menu) {        
        return (
          <div id="close">
            <button onClick={this.toggleMenu}>              
              <div className="line"/>
              <div className="line"/>
            </button>                 
          </div>
        );
      } else {
        return (
          <div id="menu">
            <button onClick={this.toggleMenu}>
              <div className="line red"/>
              <div className="line"/>
              <div className="line"/>
            </button>                 
          </div>
        );
      }          
  }
  
  toggleMenu = () => {    
    this.props.toggleMenu();
  }
}

const mapStateToProps = state => ({
    home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleMenu
      // getServices
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);
