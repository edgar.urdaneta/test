import React from "react";
import { Container, Row, Col } from "react-grid-system";
import { Carousel, Tabs, Card } from "antd";
import { bindActionCreators } from "redux";
import { getPage } from "./Home.duck";
import { connect } from "react-redux";
import "./Services.scss";

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: null,
      semiotik: null,
      methode_2: null,
      methode_3: null,
      methode_4: null,
      methode5: null
    };
  }

  componentWillMount() {
    this.props.getPage("behavioural_insights");
    this.props.getPage("semiotik");
    this.props.getPage("methode_2");
    this.props.getPage("methode_3");
    this.props.getPage("methode_4");
    this.props.getPage("methode5");
  }

  componentDidMount() {}

  componentWillReceiveProps(props) {
    this.setState({
      content: props.home.behavioural_insights,
      semiotik: props.home.semiotik,
      methode_2: props.home.methode_2,
      methode_3: props.home.methode_3,
      methode_4: props.home.methode_4,
      methode5: props.home.methode5
    });
  }

  render() {
    const content = this.state.content;
    const semiotik = this.state.semiotik;
    const methode_2 = this.state.methode_2;
    const methode_3 = this.state.methode_3;
    const methode_4 = this.state.methode_4;
    const methode5 = this.state.methode5;
    console.log("content", this.state);
    if (
      content &&
      semiotik &&
      methode_2 &&
      methode_3 &&
      methode_4 &&
      methode5
    ) {
      return (
        <div>
          <div id="services">          
            <h1 id="title"><span style={{ color: "#d11437" }}>Karmasin</span> <br />
                Behavioral
                <br /> Insights
            </h1>
            <div className="content">
              <h2 id="subtitle">Verhalten steuern? <br /> Das verhält sich so:</h2>
              <div className="columns-wrapper">
                <div className="left-column">
                  {content.main_paragraph_left}
                </div>
                <div className="right-column">
                  {content.main_paragraph_right}
                </div>
              </div>
              <div className="heading-wrapper">
                <h2 id="heading">Die Methode ist das Ziel.</h2>
              </div>
              <div className="numbers">
                <div className="number-block">                             
                  <div className="number">
                    <div className="number-cell">1</div>
                  </div>
                  <div className="block-content">
                    <div className="number-title">Semiotik.</div>
                    <div className="number-subtitle">Geheimcodes der Kommunikation.</div>                  
                    <div className="number-content"
                      dangerouslySetInnerHTML={{
                        __html: content.text_punkt_1_section
                      }}
                    />
                  </div>                               
                </div>
                <div className="number-block offset">                             
                  <div className="number">2</div>
                  <div className="block-content">
                    <div className="number-title">Verpackung.</div>
                    <div className="number-subtitle">Stilles Verkaufsgespräch.</div>                  
                    <div className="number-content"
                      dangerouslySetInnerHTML={{
                        __html: content.text_punkt_2_section
                      }}
							 //<a href="#example">Example headline</a>

                    />
                  </div>                               
                </div>
                <div className="number-block">                             
                  <div className="number">3</div>
                  <div className="block-content">
                    <div className="number-title">Marktforschung.</div>
                    <div className="number-subtitle">Hausaufgaben zuerst.</div>                  
                    <div className="number-content"
                      dangerouslySetInnerHTML={{
                        __html: content.text_punkt_3_section
                      }}
                    />
                  </div>                               
                </div>
                <div className="number-block offset">                             
                  <div className="number">4</div>
                  <div className="block-content">
                    <div className="number-title">Verhaltensökonomie.</div>
                    <div className="number-subtitle">Praktisch angewandt.</div>                  
                    <div className="number-content"
                      dangerouslySetInnerHTML={{
                        __html: content.text_punkt_4_section
                      }}
                    />
                  </div>                               
                </div>
                <div className="number-block">                             
                  <div className="number">5</div>
                  <div className="block-content">
                    <div className="number-title">Motivforschung.</div>
                    <div className="number-subtitle">Fragen ist menschlich.</div>                  
                    <div className="number-content"
                      dangerouslySetInnerHTML={{
                        __html: content.text_punkt_5_section
                      }}
                    />
                  </div>                               
                </div>
              </div>
            </div>
          </div>
          <br />
          <div id="methoden">
            <div className="methode methode1">
              <div className="block-left">
                <div className="methode-number-white">Methode 1</div>
                <div className="methode-title-white">Semiotik</div>
                <br/>
                <div className="columns">
                  <div className="paragraph-left" dangerouslySetInnerHTML={{ __html:semiotik.left_part }} />
                  <div className="paragraph-right" dangerouslySetInnerHTML={{ __html: semiotik.right_part }} />
                </div>                
                <div className="caffe_img" style={{clear: "both"}} />
              </div>
              <div className="block-right">
                <div className="big-text-methode1">Mehr über Semiotik erfahren Sie in Produkte als Botschaften</div>
                <div className="book_img" style={{clear: "both"}} />
                <div className="sm-text">
                  <div className="sm-text-title">Helene Karmasin</div>
                  <br/><br/>
                  Ein neuer Ansatz für Kommunkation, Marketing und Management.
                  <br/><br/>
                  Mit einen Vorwort von Mary Douglas.
                </div>
              </div>     
            </div>
            <br/>
            <div className="methode methode2">              
              <div className="methode-number-red">Methode 2</div>
              <div className="methode-title-red">Verpackung ist Verführung</div>
              <div className="kosmetik_img" />
              <br/>
              <div className="verpackung-text">                
                  "Ich habe mich in meinem neuen Buch, Verpackung ist
                  Verf&uuml;hrung'                
                  ausf&uuml;hrlich mit der Entschl&uuml;sselung von
                  Packungscodes auseinandergesetzt.                
                  Aus den Erkenntnisse daraus haben wir f&uuml;r Sie ein
                  Packungstestpaket geschn&uuml;rt.                
                  Ihre Kommunikationsstrategie sollte aufgehen."
              <br/>
                  Dr.Helene Karmasin
              </div>
              <br/>
              <br/>
              <div>
                <h2 className="subtext">Unser Packungstestpacket ist 2dimmensional</h2>
              </div>
              <div className="columns">
                <div className="paragraph-left" dangerouslySetInnerHTML={{ __html: methode_2.text_bottom_left }} />
                <div className="paragraph-right" dangerouslySetInnerHTML={{ __html: methode_2.text_bottom_right
 }} />
              </div>                  
            </div>
            <br/>
            <div className="methode methode3">
              <div className="block-left">
                <div className="methode-number-white">Methode 3</div>
                <div className="methode-title-white">Marktforschung</div>
                <br/>
                <div className="columns">
                  <div className="paragraph-left" dangerouslySetInnerHTML={{ __html:methode_3.left_side }} />
                  <div className="paragraph-right" dangerouslySetInnerHTML={{ __html: methode_3.middle_side }} />
                </div>                
                <div className="pie_img" style={{clear: "both"}} />
                <div className="big-number">8000</div>
                <div className="footer" dangerouslySetInnerHTML={{ __html: methode_3.right_side }} />
              </div>
              <div className="block-right">
                <div className="big-text">Man muss wissen wo man steht, um dort hinzu-kommen, wo man sich steht.</div>                
              </div>     
            </div>
            <br/>
            <div className="methode methode4">
              <div className="block-left" style={{background: "#de072f"}}>
                <div className="methode-number-white">Methode 4</div>
                <div className="methode-title-white">Verhaltensökonomie</div>
                <br/>
                <div className="columns">
                  <div className="paragraph-left" dangerouslySetInnerHTML={{ __html:methode_4.paragraph1 }} />
                  <div className="paragraph-right" dangerouslySetInnerHTML={{ __html: methode_4.paragraph4 }} />
                </div>                
                <div className="cookies_img" style={{clear: "both"}} />
                <div className="columns">
                  <div className="paragraph-left" dangerouslySetInnerHTML={{ __html:methode_4.paragraph2 + "<br/><br/>" + methode_4.paragraph3}} />                  
                  <div className="paragraph-right" dangerouslySetInnerHTML={{ __html: methode_4.paragraph5 }} />
                </div>  
              </div>
              <div className="block-right">
                <div className="big-text">Nach-<br/>zulesen<br/>für<br/>Vor-<br/>denker</div>
                <div className="book2_img" style={{clear: "both"}} />      
              </div>     
            </div>
            <br/>
            <div className="methode methode5">              
                <div className="methode-number-white">Methode 5</div>
                <div className="methode-title-white">Motivforschung</div>
                <br/>
                <div className="columns">
                  <div className="paragraph-left" dangerouslySetInnerHTML={{ __html:methode5.all_texts }} />
                  <div className="paragraph-right">
                    <div className="arrows_img" style={{clear: "both"}} />
                  </div>
                </div>
                <div>
                      <h2>Noch Fragen ?</h2>
                </div>                               
              </div>                                                 
            </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Services);
