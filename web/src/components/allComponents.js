// takes a file path e.g.: ./Foo/Bar/Baz/Qux.component.js
// and returns "Qux" as the component name
function getComponentName(file) {
	const matches = /(.*)\/(.*)\.(component)\.(jsx)$/.exec(file);
	return matches[2].replace('.container', '');
}

// find all component files in this directory e.g. filename.component.jsx
const allComponents = require.context('../components', true, /\.(component)\.(jsx)$/);
const components = {};

// dynamically find all components and export them to match [data-react-component]
allComponents.keys().forEach(file => {
	const component = allComponents(file).default;
	components[getComponentName(file)] = {
		component,
		componentPath: file,
	};
});

export default components;
