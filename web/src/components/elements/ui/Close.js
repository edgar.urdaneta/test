import React from "react";
import styled from "styled-components";
import { Button, Icon } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

const CloseButton = styled.Button`
    height: inherit;
    border: none;
    background: none;
    color: #999;
    -webkit-transition: -webkit-transform .4s ease-in-out;
    transition: transform .4s ease-in-out;
`;

class CloseBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() { }

    componentDidMount() { }

    componentWillReceiveProps(props) { }

    render() {
        return (
            <CloseButton onClick={() => this.props.toggleScreen()} type="primary" icon="close" />
        )
    }
}

export default CloseBtn;