import React from "react";
import styled from "styled-components";

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #222;
  padding-top: 20px;
  padding-bottom: 30px;
`;

const Loading = props => (
  <div>
    <Title>Loading</Title>    
  </div>
);

export default Loading;