import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware } from "react-router-redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import rootReducer from "./allReducers";

export const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const persistConfig = {
  key: "root",
  storage: storage,
  whitelist: ["auth"]
  // blacklist: ["builder"]
  // stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
};

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, initialState, composedEnhancers);

export default store;
// export default createStore(rootReducer, initialState, composedEnhancers);

export const persistor = persistStore(store);
