import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import home from "../components/home/Home.duck.js";

export default combineReducers({  
  router: routerReducer,
  home: home
});
