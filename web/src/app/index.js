import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import store, { history, persistor } from "../store/store";
import Loading from "../components/elements/loaders/Loading";
import App from "./App";
import "./App.style";

const target = document.querySelector("#root");

// Just to have a visual element to see if enviroment is test
const env =
  process.env.NODE_ENV === "test" ? <div id="top">TEST ENVIROMENT</div> : null;

render(
  <Provider store={store}>
    <PersistGate loading={<Loading />} persistor={persistor}>
      <ConnectedRouter history={history}>
        <div>
          {env}
          <App />
        </div>
      </ConnectedRouter>
    </PersistGate>
  </Provider>,
  target
);
