import React from "react";
import { Route } from "react-router-dom";
// Containers
import HomeContainer from "../components/home/Home.container";
import Menu from "../components/home/Menu";
import "./App.css";

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  componentDidMount() {}

  componentWillReceiveProps(props) {}

  render() {
    return (
      <div id="container">
        <div className="menu">
          <Menu />
        </div>
        <div className="body">
          <main id="homeLandscape">
            <Route exact path="/" component={HomeContainer} />
            <Route exact path="/services" component={HomeContainer} />
            <Route exact path="/contact" component={HomeContainer} />
            <Route exact path="/about" component={HomeContainer} />
            <Route exact path="/publications" component={HomeContainer} />            
          </main>
        </div>          
      </div>
    );
  }
}

export default App;
