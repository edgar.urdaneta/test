import { injectGlobal } from 'styled-components';

// will be injected only once globally
// eslint-disable-next-line no-unused-expressions
injectGlobal`
  html, body {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  }

  mark {
    background: transparent;
    color: inherit;
    padding: 0;
    margin: 0;
  }
`;
